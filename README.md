# Derco DevOps Infrastructure Test

Implementar servicio que consuma un api rest ya definida, y mostrar los datos a través de navegador (no importa formato).




## Contenido

Este repositorio contiene lo siguiente:

- Infraestructura como código utilizando Terraform que crea 2 instancias unidas por un Application Load Balancer junto
  con sus Security Groups
- Script en Bash que corre al momento de iniciar las intancias para realizar el pull y exponer la imagen de Docker creada en el anterior
  repositorio [Derco DevOps Test](https://gitlab.com/drandredev/derco-devops-test)
- Pipeline automatizado para conocer la infraestructura que se desea crear
- Pipeline manual para crear la infraestructura o destruirla
- Enlace para probar la aplicacion desplegada - [http://web-app-lb-1224195752.us-east-1.elb.amazonaws.com](http://web-app-lb-1224195752.us-east-1.elb.amazonaws.com)



## Usabilidad



```bash
  - Crear un issue
  - Crear un Merge Request
  - Realizar un commit a la rama creada
  - Automaticamente el Pipeline planeará nuestra infraestructura con Terraform
  - Nosotros debemos aplicar o eliminar la infraestructura en caso el Plan de Terraform sea el correcto
  - En caso de elegir aplicar el pipeline nos mostrará el enlace (DNS del Load Balancer) para poder consultarlo 
    mediante el navegador
  - En caso de elegir destruir toda la Infraestructura creada en el paso anterior se eliminará
```
    
